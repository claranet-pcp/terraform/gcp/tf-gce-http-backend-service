variable "service"{
  description = "The name of the service that this backend will belong, suffixes the names attribute on resources created by this module"
  type = "string"
}

variable "managed_instance_group1"{
  description = "The name of the first managed instance backend group"
  type = "string"
}

variable "managed_instance_group2"{
  description = "The name of the second managed instance backend group"
  type = "string"
}

variable "managed_instance_group3"{
  description = "The name of the third managed instance backend group"
  type = "string"
}
