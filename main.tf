resource "google_compute_backend_service" "backend_service" {
  name        = "backend-${var.service}"
  timeout_sec = 10

  backend {
    group = "${var.managed_instance_group1}"
  }

  backend {
    group = "${var.managed_instance_group2}"
  }

  backend {
    group = "${var.managed_instance_group3}"
  }

  health_checks = ["${google_compute_http_health_check.health_check.self_link}"]
}

resource "google_compute_http_health_check" "health_check" {
  name               = "healthcheck-${var.service}"
  request_path       = "/"
  check_interval_sec = 5
  timeout_sec        = 5
}
