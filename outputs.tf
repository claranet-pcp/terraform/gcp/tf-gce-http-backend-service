output "backend_service_name" {
  value = "${google_compute_backend_service.backend_service.name}"
}
output "backend_service_link" {
  value = "${google_compute_backend_service.backend_service.self_link}"
}
output "http_health_check_link" {
  value = "${google_compute_http_health_check.health_check.self_link}"
}
